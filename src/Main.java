import java.util.Scanner;

public class Main {
	private static final Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		do {
			// get parameters from user input
			double a = getDouble("Please enter a number value for A:");
			double b = getDouble("Please enter a number value for B:");
			double c = getDouble("Please enter a number value for C:");
			
			try {
				// calculate larger root solution
				double rootResult = root(a, b, c);
				
				// print result
				System.out.println(String.format("The larger root solution for %sX^2%s%sX%s%s ==> X = %s", a, b < 0 ? "" : "+", b, c < 0 ? "" : "+", c, rootResult));
			} catch (IllegalArgumentException ex) {
				// print error
				System.out.println("Invalid parameters: " + ex.getMessage());
			}
			
			// ask and repeat
		} while (askTryAgain());
	}
	
	private static Boolean askTryAgain() {
		System.out.print("Would you like to try another equation set? ");
		String tryAgain = scanner.nextLine();
		if (tryAgain != null) {
			switch(tryAgain.toLowerCase()) {
				case "yes":
				case "y":
					System.out.println();
					return true;
				default:
					System.out.println("Bye!");
					return false;
			}
		}
		return true;
	}

	/**
	 * Gets a double value from the user. Repeats until a double is provided.
	 * @param prompt - Question to prompt the user
	 * @return A double value
	 */
	private static double getDouble(String prompt) {
		while (true) {
			System.out.print(prompt + " ");
			try {
				String line = scanner.nextLine();
				return Double.parseDouble(line);
			} catch (Exception ex) {
				System.out.println("Invalid number, please try again...");
			}
		}
	}

	/**
	 * Returns the larger of the two roots of the quadratic equation
	 * A*x*x + B*x + C = 0, provided it has any roots.  If A == 0 or
	 * if the discriminant, B*B - 4*A*C, is negative, then an exception
	 * of type IllegalArgumentException is thrown.
	 */
	static public double root( double A, double B, double C ) throws IllegalArgumentException {
	    if (A == 0) {
	      throw new IllegalArgumentException("A can't be zero.");
	    }
	    else {
	       double disc = B*B - 4*A*C;
	       if (disc < 0) throw new IllegalArgumentException("Discriminant < zero.");
	       return  (-B + Math.sqrt(disc)) / (2*A);
	    }
	}
}
